class Activity:
    def bark(self):
        print("bark bark")
    def meow(self):
        print("meow meow")

class Dog(Activity):
    pass

class Cat(Activity):
    pass

dog = Dog()
dog.bark()

cat = Cat()
cat.meow()