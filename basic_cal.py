class Calculator:

    def add(self, a, b):
        return a + b

    def sub(self, a, b):
        return a - b
    
    def mul(self, a, b):
        return a * b

    def div(self, a, b):
        try:
            return a / b
        except ZeroDivisionError:
            return "It is not possible to divide by zero."

class SuperCalculator(Calculator):

    def add(self, a, b, c):
        return a + b + c
    def sq(self, a):
        return a * a
    def cube(self, a):
        return a * a * a

my_calc = SuperCalculator()

temp = my_calc.add(23, 23, 11)
print(temp)
temp = my_calc.sub(45, 35)
print(temp)
temp = my_calc.mul(45, 12)
print(temp)
temp = my_calc.div(740, 24)
print(temp)
temp = my_calc.sq(88)q1qwe
print(temp)
temp = my_calc.cube(33)
print(temp)